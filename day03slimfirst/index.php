<?php
require_once 'vendor/autoload.php';

// Create and configure Slim app
$config = ['settings' => [
    'addContentLengthHeader' => false,
    'displayErrorDetails' => true
]];
$app = new \Slim\App($config);

// Fetch DI Container
$container = $app->getContainer();

// Register Twig View helper
$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig(dirname(__FILE__) . '/templates', [
        'cache' => dirname(__FILE__) . '/tmplcache',
        'debug' => true, // This line should enable debug mode
    ]);
    //
    $view->getEnvironment()->addGlobal('test1','VALUE');
    // Instantiate and add Slim specific extension
    $router = $c->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
    return $view;
};

// Define app routes
$app->get('/hello/{name}/{age:[0-9]+}', function ($request, $response, $args) {
    $name = $args['name'];
    $age = $args['age'];
    return $this->view->render($response, 'hello.html.twig', ['age' => $age, 'name' => $name]);
    // return $response->write("<p>Hello $name ,</br> you are $age years old.</p>");
});

// Run app
$app->run();